from django.test import TestCase
from .models import (
    Teacher,
    Student,
    ClassRoom,
    Quiz,
    QuizQuestionList,
    QuestionChoice,
    Question,
)

# Assumptions:
# 1. Only one (main) teacher per classroom
# 2. Quizzes can only be created for a single classroom / subject
# 3. Classroom has a start and end date
# 4. Quizzes can only be graded once assigned student has submitted the quiz
# 5. Only quizzes that are graded count towards student's grade
# 6. Assumes that there can be list of questions that teachers can create quizzes out of and 
#    can create new ones that are shared across the classroom / subject
# 7. Teacher can only view grades of students in classrooms that they are teaching in
# 8. When grading, each question is weighted equally.
# 9. Students can submit quizzes without filiing out all the answers
# 10. Ability to see previous selected answers (transaction of students answering a question) 
#     is not required.

"""
Tests to Write:
There are Teachers
There are Students
Students are in classes that teachers teach
Teachers can create multiple quizzes with many questions (each question is multiple choice)
Teachers can assign quizzes to students
Students solve/answer questions to complete the quiz, but they don't have to complete it at
once. (Partial submissions can be made).
Quizzes need to get graded
For each teacher, they can calculate each student's total grade accumulated over a semester
for their classes
"""

class BaseTestSetup(TestCase):
    def setUp(self):
        self.question = Question.objects.create(question_text="Test question?")
        for answer in ["Answer 1", "Answer 2", "Answer 3", "Answer 4"]:
            qc = QuestionChoice.objects.create(label=answer, question=self.question)
            qc.correct_answer = answer == "Answer 4"
            qc.save()
        
        self.question2 = Question.objects.create(question_text="Test question2?")
        for answer in ["Answer 1", "Answer 2", "Answer 3", "Answer 4"]:
            qc = QuestionChoice.objects.create(label=answer, question=self.question2)
            qc.correct_answer = answer == "Answer 1"
            qc.save()
        
        self.teacher = Teacher.objects.create(first_name="Teacher1", last_name="Test")
        self.student = Student.objects.create(first_name="Student1", last_name="Test")
    

class ProjectTests(BaseTestSetup):
    def test_create_student(self):
        self.assertTrue(isinstance(self.student, Student))
    
    def test_create_teacher(self):
        self.assertTrue(isinstance(self.teacher, Teacher))
    
    def test_students_in_classroom(self):
        classroom = ClassRoom.objects.create(name="Class Room 1", teacher=self.teacher)

        classroom.add_student(self.student)
        self.assertTrue(self.student in classroom.students.all())
    
    def test_create_quiz_with_multiple_questions(self):
        classroom = ClassRoom.objects.create(name="Class Room 2", teacher=self.teacher)

        quiz = classroom.create_quiz(
            question_list=[self.question, self.question2],
            assigned_to=self.student,
        )

        self.assertEqual(quiz.questions.count(), 2)
        # Teachers can assign quizzes to students only at time of creating quiz
        self.assertEqual(quiz.assigned_to, self.student)

    def test_student_answer_questions(self):
        classroom = ClassRoom.objects.create(name="Class Room 3", teacher=self.teacher)

        quiz = classroom.create_quiz(
            question_list=[self.question, self.question2],
            assigned_to=self.student,
        )
        first_question = quiz.questions.first()
        quiz.answer_question(first_question.id, first_question.choices.last().id)

        self.assertEqual(quiz.status, Quiz.STARTED)
        self.assertEqual(quiz.correct_answer_count, 1)
    
    def test_grade_quiz(self):
        classroom = ClassRoom.objects.create(name="Class Room 3", teacher=self.teacher)

        classroom.add_student(self.student)

        quiz = classroom.create_quiz(
            question_list=[self.question, self.question2],
            assigned_to=self.student,
        )
        first_question = quiz.questions.first()
        quiz.answer_question(first_question.id, first_question.choices.last().id)
        
        last_question = quiz.questions.last()
        quiz.answer_question(last_question.id, last_question.choices.first().id)
        
        # Teachers should not be able to grade a 
        # quiz without student submitting it first
        self.assertRaises(Exception, quiz.grade)

        quiz.submit()
        quiz.grade()
        
        self.assertEqual(quiz.status, Quiz.GRADED)
        
        # Test calculating total grade for student for class
        final_grade = self.teacher.calculate_total_grade_for_student(classroom_id=classroom.id, student_id=self.student.id)
        self.assertEqual(final_grade, 100)
        
        

