# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist

class BaseClass(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False) 
    is_removed = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(null=True, default=None)

    class Meta:
        abstract = True
        ordering = ['created_at']

class Person(BaseClass):
    first_name = models.TextField(max_length=254)
    last_name = models.TextField(max_length=254)

    class Meta:
        abstract = True

class Teacher(Person):
    def __unicode__(self): 
        return "Teacher: {0} {1}".format(self.first_name, self.last_name)

    def calculate_total_grade_for_student(self, classroom_id, student_id):
        try:
            classroom = self.classrooms.get(id=classroom_id)
        except ObjectDoesNotExist:
            raise Exception('Teacher is not authorized to grade for selected classroom')
        
        return classroom.calculate_total_grade_for_student(student_id)

class Student(Person):
    def __unicode__(self): 
        return "Student: {0} {1}".format(self.first_name, self.last_name)

class ClassRoom(BaseClass):
    name = models.TextField()
    teacher = models.ForeignKey(Teacher, null=True, related_name="classrooms", on_delete=models.SET_NULL)
    end_date = models.DateTimeField(blank=True, null=True)
    students = models.ManyToManyField(Student, through="ClassRoomStudentList", related_name="classrooms")

    def add_student(self, student):
        return ClassRoomStudentList.objects.create(student=student, classroom=self)

    def create_quiz(self, question_list, assigned_to):
        new_quiz = Quiz.objects.create(classroom=self, 
            assigned_to=assigned_to,
            created_by=self.teacher,
        )
        for question in question_list: 
            new_quiz.add_question(question)
        return new_quiz
    
    def calculate_total_grade_for_student(self, student_id):
        try:
            student = self.students.get(id=student_id)
        except ObjectDoesNotExist:
            raise Exception('Student does not belong in selected classroom')
        
        total_questions_count = 0
        total_correct_count = 0
        graded_quizzes = self.quizzes.filter(assigned_to=student, status=Quiz.GRADED)
        if not graded_quizzes.exists():
            raise Exception('Student does not have any graded quizzes')
        for quiz in graded_quizzes:
            total_questions_count += quiz.questions_count
            total_correct_count += quiz.correct_answer_count
        return (100.0 * total_questions_count / total_correct_count)

class ClassRoomStudentList(BaseClass):
    student = models.OneToOneField(Student, on_delete=models.CASCADE)
    classroom = models.OneToOneField(ClassRoom, on_delete=models.CASCADE)

class Question(BaseClass):
    question_text = models.TextField()

class QuestionChoice(BaseClass):
    question = models.ForeignKey(Question, related_name="choices", on_delete=models.CASCADE)
    label = models.TextField(max_length=254)
    correct_answer = models.BooleanField(default=False)

class Quiz(BaseClass):
    ASSIGNED = 'AS'
    STARTED = 'ST'
    SUBMITTED = 'SU'
    GRADED = 'GR'

    STATUSES = (
        (ASSIGNED, 'Assigned'),
        (STARTED, 'Started'),
        (SUBMITTED, 'Submitted'),
        (GRADED, 'Graded'),
    )

    classroom = models.ForeignKey(ClassRoom, related_name="quizzes", on_delete=models.CASCADE)
    questions = models.ManyToManyField(Question, through="QuizQuestionList", related_name="quizzes")
    assigned_to = models.OneToOneField(Student, on_delete=models.CASCADE)
    created_by = models.OneToOneField(Teacher, on_delete=models.CASCADE)
    started_at = models.DateTimeField(blank=True, null=True)
    submitted_at = models.DateTimeField(blank=True, null=True)
    graded_at = models.DateTimeField(blank=True, null=True)
    status = models.CharField(choices=STATUSES, default=ASSIGNED, max_length=2)
    answers = JSONField(default=dict)

    @property
    def questions_count(self):
        return self.questions.count()

    @property
    def correct_answer_count(self):
        correct_count = 0
        for q in self.questions.all():
            if self.answers.get(str(q.id)):
                choice = QuestionChoice.objects.get(id=self.answers[str(q.id)])
                if choice.correct_answer:
                    correct_count += 1
        return correct_count

    @property
    def score_percentage(self):
        return (100.0 * self.correct_answer_count / self.questions_count)
    
    def start(self):
        self.started_at = timezone.now()
        self.status = self.STARTED
        self.save()
    
    def submit(self):
        self.submitted_at = timezone.now()
        self.status = self.SUBMITTED
        self.save()
    
    # Answer dict data:
    # { "question_id": "choice_id" }
    def answer_question(self, question_id, choice_id):
        if self.status == self.ASSIGNED:
            self.status = self.STARTED
        self.answers[str(question_id)] = str(choice_id)
        self.save()
    
    def grade(self):
        if self.status != self.SUBMITTED:
            raise Exception('Quiz not submitted by student yet.')
        self.grade_at = timezone.now()
        self.status = self.GRADED
        self.save()
    
    def add_question(self, question):
        return QuizQuestionList.objects.create(question=question, quiz=self)

# Ordered by created_at so questions can be ordered by
# the order questions were added to the quiz
class QuizQuestionList(BaseClass):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)